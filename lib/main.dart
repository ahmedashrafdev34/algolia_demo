import 'package:algolia_demo/search/search_imports.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Algolia Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: SearchView(),
    );
  }
}
