part of 'search_imports.dart';

var applicationID = 'RD12RMDVFH';
var apiKey = '1d29b371bbfbbf68dae68941e17ea456';
var indexName = 'Documents';

// test data include data

// var applicationID= 'latency';
// var apiKey=  '927c3fe76d4b52c5a2912973f35a3077';
// var indexName= 'STAGING_native_ecom_demo_products';

class SearchData {
  final _searchTextController = TextEditingController();

  final _productsSearcher = HitsSearcher(
    applicationID: applicationID,
    apiKey: apiKey,
    indexName: indexName,
  );

  Stream<SearchMetadata> get _searchMetadata =>
      _productsSearcher.responses.map(SearchMetadata.fromResponse);

  final PagingController<int, Product> _pagingController =
      PagingController(firstPageKey: 0);

  Stream<HitsPage> get _searchPage =>
      _productsSearcher.responses.map(HitsPage.fromResponse);
}

class SearchMetadata {
  final int nbHits;

  const SearchMetadata(this.nbHits);

  factory SearchMetadata.fromResponse(SearchResponse response) =>
      SearchMetadata(response.nbHits);
}

class Product {
  final String name;

  Product(this.name);

  static Product fromJson(Map<String, dynamic> json) {
    return Product(json['name']);
  }
}

class HitsPage {
  const HitsPage(this.items, this.pageKey, this.nextPageKey);

  final List<Product> items;
  final int pageKey;
  final int? nextPageKey;

  factory HitsPage.fromResponse(SearchResponse response) {
    final items = response.hits.map(Product.fromJson).toList();
    final isLastPage = response.page >= response.nbPages;
    final nextPageKey = isLastPage ? null : response.page + 1;
    return HitsPage(items, response.page, nextPageKey);
  }
}
