part of 'search_imports.dart';

class SearchView extends StatefulWidget {
  @override
  State<SearchView> createState() => _SearchViewState();
}

class _SearchViewState extends State<SearchView> {
  final SearchData model = SearchData();

  @override
  void initState() {
    super.initState();
    model._searchTextController.addListener(() {
      model._productsSearcher.query(model._searchTextController.text);
    });
    model._searchPage.listen((page) {
      if (page.pageKey == 0) {
        model._pagingController.refresh();
        model._pagingController.appendLastPage(page.items);
      } else {
        model._pagingController.appendPage(page.items, page.nextPageKey);
      }
    }).onError((error) {
      model._pagingController.error = error;
    });

    model._pagingController.addPageRequestListener((pageKey) {

      model._productsSearcher.applyState((state) {
        return state.copyWith(page: pageKey);
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Algolia & Flutter'),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 44,
              child: TextField(
                controller: model._searchTextController,
                decoration: const InputDecoration(
                  border: InputBorder.none,
                  hintText: 'Enter a search term',
                  prefixIcon: Icon(Icons.search),
                ),
              ),
            ),
            StreamBuilder<SearchMetadata>(
              stream: model._searchMetadata,
              builder: (context, snapshot) {
                if (!snapshot.hasData) {
                  return const SizedBox.shrink();
                }
                return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text('${snapshot.data!.nbHits} hits'),
                );
              },
            ),
            Expanded(
              child: _hits(context),
            )
          ],
        ),
      ),
    );
  }

  Widget _hits(BuildContext context) => PagedListView<int, Product>(
        pagingController: model._pagingController,
        builderDelegate: PagedChildBuilderDelegate<Product>(
          noItemsFoundIndicatorBuilder: (_) => const Center(
            child: Text('No results found'),
          ),
          itemBuilder: (_, item, __) => Container(
            color: Colors.white,
            height: 80,
            padding: const EdgeInsets.all(8),
            child: Row(
              children: [Expanded(child: Text(item.name))],
            ),
          ),
        ),
      );

  @override
  void dispose() {
    model._searchTextController.dispose();
    model._productsSearcher.dispose();
    model._pagingController.dispose();
    super.dispose();
  }
}
